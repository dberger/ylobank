#!/usr/bin/ruby

commandsJs = [];
commandsPhp = [];
threads = []

commandsPhp.push("composer install --no-interaction --no-progress")  # Install symfony
commandsJs.push("yarn") # Install node_modules
commandsJs.push("yarn encore dev") # Test build scss, js
commandsPhp.push("php bin/console doctrine:migrations:migrate --env=test -n") # Test database migrations
commandsPhp.push("php bin/console doctrine:fixtures:load --append") # Test fixtures
commandsPhp.push("php bin/console doctrine:schema:validate")  # Test schema validation
commandsPhp.push("php bin/console server:run &")  # For Functionnal tests
commandsPhp.push("php bin/phpunit tests/")  # Unit Tests

threads << Thread.new { commandsJs.each { |cmd| system(cmd) || raise("Exit Code : 1") } } 
threads << Thread.new { commandsPhp.each { |cmd| system(cmd) || raise("Exit Code : 1") } }

threads.each do |thr| 
	thr.join
	thr.status.nil? ? exit!() : next 
end
