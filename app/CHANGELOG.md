# Changelog

Tous les changements notables à  ce projet seront documentés dans ce fichier.
Le format est basé sur [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
et ce projet adhère à  la version sémantique. [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.0.1] 07-08-2019
 - Init project

