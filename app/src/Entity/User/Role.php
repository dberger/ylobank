<?php

declare(strict_types=1);

namespace App\Entity\User;

use Doctrine\ORM\Mapping as ORM;
use MsgPhp\User\Entity\Role as BaseRole;

/**
 * Class Role
 * @package App\Entity\User
 * @ORM\Entity()
 *
 * @final
 */
class Role extends BaseRole
{
    const ROLE_USER = 'ROLE_USER';
    const ROLE_ADMIN = 'ROLE_ADMIN';

    /** @ORM\Id() @ORM\Column(length=191) */
    private $name;

    /**
     * Role constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public static function getRoles()
    {
        return [self::ROLE_USER, self::ROLE_ADMIN];
    }
}
