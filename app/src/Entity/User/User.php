<?php

namespace App\Entity\User;

use App\Entity\Account;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use MsgPhp\User\Entity\User as BaseUser;
use MsgPhp\User\UserIdInterface;
use MsgPhp\Domain\Event\DomainEventHandlerInterface;
use MsgPhp\Domain\Event\DomainEventHandlerTrait;
use MsgPhp\User\Entity\Credential\EmailPassword;
use MsgPhp\User\Entity\Features\EmailPasswordCredential;
use MsgPhp\User\Entity\Features\ResettablePassword;
use MsgPhp\User\Entity\Fields\RolesField;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class User
 * @package App\Entity\User
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User extends BaseUser implements DomainEventHandlerInterface, UserInterface
{
    use DomainEventHandlerTrait;
    use EmailPasswordCredential;
    use ResettablePassword;
    use RolesField;

    /** @ORM\Id() @ORM\GeneratedValue() @ORM\Column(type="msgphp_user_id", length=191) */
    private $id;

    /**
     * @var Account[]
     * @ORM\OneToMany(targetEntity="App\Entity\Account", mappedBy="user", cascade={"persist", "remove"})
     */
    private $accounts;

    public function __construct(UserIdInterface $id, string $email, string $password)
    {
        $this->id = $id;
        $this->credential = new EmailPassword($email, $password);
        $this->accounts = new ArrayCollection();
    }

    public function getId(): UserIdInterface
    {
        return $this->id;
    }

    /**
     * @return Account[]
     */
    public function getAccounts(): PersistentCollection
    {
        return $this->accounts;
    }

    /**
     * Add a account
     * @param Account $account
     * @return $this
     */
    public function addAccount(Account $account)
    {
        if (!$this->accounts->contains($account)) {
            $account->setUser($this);
            $this->accounts->add($account);
        }
        return $this;
    }

    /**
     * Remove a account
     * @param Account $account
     * @return $this
     */
    public function removeAccount(Account $account)
    {
        if ($this->accounts->contains($account)) {
            $this->accounts->removeElement($account);
        }
        return $this;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        // TODO: Implement getUsername() method.
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * Return the sum of all the user's account
     * @return float
     */
    public function getTotalSolde(): float
    {
        $solde = 0;
        foreach ($this->accounts as $account) {
            $solde += $account->getSolde();
        }
        return round($solde, 2);
    }
}
