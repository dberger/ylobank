<?php


namespace App\Entity\Transaction;

use App\Entity\Account;
use App\Entity\TransactionType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Transaction
 * @package App\Entity\Transaction
 * @ORM\Entity(repositoryClass="App\Repository\TransactionRepository")
 * @ORM\Table(name="transaction",
 *     options={"comment":"Table of transactions"}
 * )
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({
 *     "base" = "Transaction",
 *     "credit" = "Credit",
 *     "debit" = "Debit",
 *     })
 */
abstract class Transaction
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"comment":"Id of transaction"})
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="decimal", precision=7, scale=2)
     * @Assert\NotNull()
     */
    private $amount;

    /**
     * @var Account
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="App\Entity\Account", inversedBy="transactions")
     * @ORM\JoinColumn(name="id_account", referencedColumnName="id", nullable=false)
     */
    private $account;

    /**
     * @var \DateTime
     * @Assert\NotNull()
     * @ORM\Column(name="date_transaction", type="datetime")
     */
    private $dateTransaction;

    /**
     * @var TransactionType
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="App\Entity\TransactionType")
     * @ORM\JoinColumn(name="id_transaction_type", referencedColumnName="id", nullable=false)
     */
    private $transactionType;

    /**
     * Transaction constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->dateTransaction = new \DateTime();
    }


    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return Transaction
     */
    public function setAmount(float $amount): Transaction
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return Account
     */
    public function getAccount(): Account
    {
        return $this->account;
    }

    /**
     * @param Account $account
     * @return Transaction
     */
    public function setAccount(?Account $account): Transaction
    {
        $this->account = $account;
        if ($account instanceof Account) {
            $account->addTransaction($this);
        }
        return $this;
    }

    /**
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return TransactionType
     */
    public function getTransactionType(): TransactionType
    {
        return $this->transactionType;
    }

    /**
     * @param TransactionType $transactionType
     * @return Transaction
     */
    public function setTransactionType(TransactionType $transactionType): Transaction
    {
        $this->transactionType = $transactionType;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTransaction(): \DateTime
    {
        return $this->dateTransaction;
    }

}
