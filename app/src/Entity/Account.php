<?php


namespace App\Entity;

use App\Entity\Favorite\T2CFavorite;
use App\Entity\Transaction\Credit;
use App\Entity\Transaction\Debit;
use App\Entity\Transaction\Transaction;
use App\Entity\User\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Account
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\AccountRepository")
 */
class Account
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var AccountType
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="App\Entity\AccountType")
     * @ORM\JoinColumn(name="id_account_type", referencedColumnName="id", nullable=false)
     */
    private $accountType;

    /**
     * @var float
     *
     * @ORM\Column(name="solde", type="decimal", precision=7, scale=2)
     */
    private $solde = 0;

    /**
     * @var User
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User", inversedBy="accounts")
     * @ORM\JoinColumn(name="id_user", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @var Transaction[]
     * @ORM\OneToMany(targetEntity="App\Entity\Transaction\Transaction", mappedBy="account", cascade={"persist", "remove"})
     */
    private $transactions;

    /**
     * Account constructor.
     */
    public function __construct()
    {
        $this->transactions = new ArrayCollection();
    }


    /**
     * @return AccountType
     */
    public function getAccountType(): AccountType
    {
        return $this->accountType;
    }

    /**
     * @param AccountType $accountType
     * @return Account
     */
    public function setAccountType(AccountType $accountType): Account
    {
        $this->accountType = $accountType;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Account
     */
    public function setUser(User $user): Account
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Transaction[]
     */
    public function getTransactions(): PersistentCollection
    {
        return $this->transactions;
    }

    /**
     * @param Transaction[] $transactions
     * @return Account
     */
    public function setTransactions(array $transactions): Account
    {
        $this->transactions = $transactions;
        return $this;
    }

    /**
     * Add a transaction
     * @param Transaction $transaction
     * @return Account
     */
    public function addTransaction(Transaction $transaction): Account
    {
        if (!$this->transactions->contains($transaction)) {
            if ($transaction->getAccount() !== $this) {
                $transaction->setAccount($this);
            }
            $this->transactions->add($transaction);
            $this->solde = $transaction instanceof Credit ?
                $this->solde + $transaction->getAmount()
                : (
                $transaction instanceof Debit ? $this->solde - $transaction->getAmount() : $this->solde
                );
        }
        return $this;
    }

    /**
     * Remove a transaction
     * @param Transaction $transaction
     * @return Account
     */
    public function removeTransaction(Transaction $transaction): Account
    {
        if ($this->transactions->contains($transaction)) {
            $transaction->setAccount(null);
            $this->transactions->removeElement($transaction);
            $this->solde = $transaction instanceof Credit ?
                $this->solde - $transaction->getAmount()
                : (
                $transaction instanceof Debit ? $this->solde + $transaction->getAmount() : $this->solde
                );
        }
        return $this;
    }

    /**
     * @return float
     */
    public function getSolde(): float
    {
        return $this->solde;
    }

    /**
     * @param float $solde
     * @return Account
     */
    public function setSolde(float $solde): Account
    {
        $this->solde = $solde;
        return $this;
    }

}
