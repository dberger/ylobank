<?php

namespace App\Repository;

use App\Entity\TransactionType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * @package App\Repository
 */
final class TransactionTypeRepository extends EntityRepository
{
    /**
     * Return only the objects that are for the debit form
     * @param string $type
     * @return Query
     */
    public function getTypes(string $type = null)
    {
        $qb = $this->createQueryBuilder('tt');
        if (in_array($type, [TransactionType::SEARCH_CREDIT, TransactionType::SEARCH_DEBIT], true)) {
            $qb
                ->andWhere('tt.code IN (:codes)')
                ->setParameter('codes', array_keys($type === TransactionType::SEARCH_DEBIT ? TransactionType::getDebitTypes() : TransactionType::getCreditTypes()));
        }
        return $qb->getQuery();
    }
}
