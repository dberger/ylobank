<?php

namespace App\Repository;

use App\Entity\User\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * @package App\Repository
 */
final class AccountRepository extends EntityRepository
{
    /**
     * @param User $user
     * @param int $accountId
     * @return Query
     */
    public function getAccountWithUserConstraint(User $user, int $accountId)
    {
        return $this
            ->createQueryBuilder('a')
            ->andWhere('a.user = :user')
            ->andWhere('a.id = :account_id')
            ->setParameter('user', $user)
            ->setParameter('account_id', $accountId)
            ->getQuery();
    }
}
