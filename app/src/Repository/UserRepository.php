<?php

namespace App\Repository;

use App\Model\User\UserSearch;
use Doctrine\ORM\EntityRepository;

/**
 * Class UserRepository
 * @package App\Repository
 */
final class UserRepository extends EntityRepository
{
    /**
     * @param UserSearch $search
     * @return \Doctrine\ORM\Query
     */
    public function getSearchQuery(UserSearch $search)
    {
        $query = $this->createQueryBuilder('u');

        if ($search->getId()) {
            $query->andWhere('u.id = :id')
                ->setParameter('id', $search->getId());
        }

        return $query->getQuery();
    }
}
