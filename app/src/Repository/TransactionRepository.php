<?php

namespace App\Repository;

use App\Entity\Account;
use App\Entity\TransactionType;
use App\Entity\User\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Class TransactionRepository
 * @package App\Repository
 */
final class TransactionRepository extends EntityRepository
{
    const KEY_SUM = 'sum';
    const KEY_NAME = 'name';

    /**
     * @param User $user
     * @param bool $isIncome
     * @return \Doctrine\ORM\Query
     * @throws \Exception
     */
    public function getMonthlyIncomeOrOutcomeOfUser(User $user, $isIncome = true): Query
    {
        $query = $this->createQueryBuilder('t');

        $currentMonth = (new \DateTime())->format('Y-m');

        $query
            ->select('SUM(t.amount)')
            ->andWhere('t.dateTransaction LIKE :current_month')
            ->setParameter('current_month', "{$currentMonth}%")
            ->innerJoin('t.account', 'a')
            ->andWhere('a.user = :user')
            ->setParameter('user', $user)
            ->innerJoin('t.transactionType', 'tt')
            ->andWhere('tt.code IN(:codes)')
            ->setParameter('codes', array_keys($isIncome ? TransactionType::getCreditTypes() : TransactionType::getDebitTypes()));

        return $query->getQuery();
    }

    /**
     * @param \DateTime $periodStart
     * @param \DateTime $periodEnd
     * @param Account $account
     * @return \Doctrine\ORM\Query
     */
    public function getSpendingsByCategoryInPeriodOfAccount(Account $account, ?\DateTime $periodStart, ?\DateTime $periodEnd): Query
    {
        $qb = $this
            ->createQueryBuilder('t')
            ->select('SUM(t.amount) as ' . self::KEY_SUM . ', tt.name as ' . self::KEY_NAME)
            ->innerJoin('t.transactionType', 'tt')
            ->andWhere('t.account = :account')
            ->andWhere('tt.code IN(:codes)')
            ->setParameter('account', $account)
            ->setParameter('codes', array_keys(TransactionType::getDebitTypes()))
            ->groupBy('tt.name');

        if ($periodStart instanceof \DateTime && $periodEnd instanceof \DateTime) {
            $qb
                ->andWhere('t.dateTransaction BETWEEN :period_start AND :period_end')
                ->setParameter('period_start', $periodStart)
                ->setParameter('period_end', $periodEnd);
        }

        return $qb->getQuery();

    }
}
