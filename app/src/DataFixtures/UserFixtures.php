<?php

namespace App\DataFixtures;

use App\DataProviders\UserFactory;
use App\Entity\Account;
use App\Entity\AccountType;
use App\Entity\Favorite\T2CFavorite;
use App\Entity\Stops\T2CStop;
use App\Entity\Transaction\Credit;
use App\Entity\Transaction\Debit;
use App\Entity\TransactionType;
use App\Services\AccountType\AccountTypeSearchProvider;
use App\Services\T2CStop\T2CStopSearchProvider;
use App\Services\TransactionType\TransactionTypeSearchProvider;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use MsgPhp\User\UserId;

/**
 * Class UserFixtures
 * @package App\DataFixtures
 */
final class UserFixtures extends Fixture
{

    /**
     * @var UserFactory
     */
    private $userFactory;

    /**
     * @var TransactionTypeSearchProvider
     */
    private $transactionTypeSearchProvider;
    /**
     * @var AccountTypeSearchProvider
     */
    private $accountTypeSearchProvider;

    /**
     * UserFixtures constructor.
     * @param UserFactory $userFactory
     * @param TransactionTypeSearchProvider $transactionTypeSearchProvider
     * @param AccountTypeSearchProvider $accountTypeSearchProvider
     */
    public function __construct(UserFactory $userFactory, TransactionTypeSearchProvider $transactionTypeSearchProvider, AccountTypeSearchProvider $accountTypeSearchProvider)
    {
        $this->userFactory = $userFactory;
        $this->transactionTypeSearchProvider = $transactionTypeSearchProvider;
        $this->accountTypeSearchProvider = $accountTypeSearchProvider;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();

        /** @var TransactionType[] $transactionTypes */
        $transactionTypes = $this->transactionTypeSearchProvider->findAll();
        /** @var AccountType[] $accountTypes */
        $accountTypes = $this->accountTypeSearchProvider->findAll();

        for ($i = 0; $i < 10; $i++) {
            $user = $this->userFactory->createUser(new UserId(), $faker->email, 'user');

            foreach ($accountTypes as $accountType) {
                $account = new Account();
                $account->setUser($user);
                $account->setAccountType($accountType);
                $account->setSolde($faker->randomFloat(2, 100, 1600));

                foreach ($transactionTypes as $transactionType) {
                    if (key_exists($transactionType->getCode(), TransactionType::getCreditTypes())) {
                        $transaction = new Credit();
                    } else {
                        $transaction = new Debit();
                    }

                    $transaction->setTransactionType($transactionType);
                    $transaction->setAmount($faker->randomFloat(2, 1, 150));
                    $transaction->setAccount($account);
                    $account->addTransaction($transaction);
                }

                $user->addAccount($account);
            }
            $manager->persist($user);
        }

        $manager->flush();
    }
}
