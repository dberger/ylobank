<?php


namespace App\Controller;


use App\Model\Response\AjaxResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends AbstractController
{
    /**
     * Return an error when user is not logged
     * @return AjaxResponse
     */
    protected function userNotLoggedIn()
    {
        return (new AjaxResponse())
            ->setFlashMessage('The user is not logged in')
            ->setFlashType('error')
            ->formatResponseContent();
    }

    /**
     * Return a default 404 error
     * @return AjaxResponse
     */
    protected function throw404()
    {
        return (new AjaxResponse())
            ->setFlashMessage('Not Found')
            ->setFlashType('error')
            ->setStatusCode(Response::HTTP_NOT_FOUND)
            ->formatResponseContent();
    }

    /**
     * Return a default 403 error
     * @return AjaxResponse
     */
    protected function throw403()
    {
        return (new AjaxResponse())
            ->setFlashMessage('Access Denied')
            ->setFlashType('error')
            ->setStatusCode(Response::HTTP_FORBIDDEN)
            ->formatResponseContent();
    }
}
