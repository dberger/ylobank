<?php

namespace App\Controller;

use App\Model\Response\AjaxResponse;
use App\Services\Account\AccountSearchProvider;
use App\Services\Transaction\TransactionCrudManager;
use App\Services\Transaction\TransactionFactory;
use App\Services\TransactionType\TransactionTypeSearchProvider;
use App\Services\User\UserSearchProvider;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use MsgPhp\User\Infra\Security\SecurityUser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TransactionController
 * @package App\Controller
 * @Route("/transaction")
 */
final class TransactionController extends BaseController
{
    /**
     * @Route("/add/", name="rest_add_transaction", methods={"POST"})
     * Add a transaction
     * @param Request $request
     * @param UserSearchProvider $userSearchProvider
     * @param AccountSearchProvider $accountSearchProvider
     * @param TransactionTypeSearchProvider $transactionTypeSearchProvider
     * @param TransactionFactory $transactionFactory
     * @param TransactionCrudManager $transactionCrudManager
     * @return AjaxResponse
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws \Exception
     */
    public function addAction
    (
        Request $request,
        UserSearchProvider $userSearchProvider,
        AccountSearchProvider $accountSearchProvider,
        TransactionTypeSearchProvider $transactionTypeSearchProvider,
        TransactionFactory $transactionFactory,
        TransactionCrudManager $transactionCrudManager
    )
    {
        /** @var SecurityUser $msgUser */
        $msgUser = $this->getUser();

        if (!$msgUser instanceof SecurityUser) {
            return $this->userNotLoggedIn();
        }

        /** @var \App\Entity\User\User $user */

        if (($user = $userSearchProvider->find($msgUser->getUserId()->toString()) ) && $request->get('account') !== null && $request->get('type') !== null && $request->get('amount') !== null) {
            if (($account = $accountSearchProvider->getAccountWithUserConstraint($user, intval($request->get('account')))) && $transactionType = $transactionTypeSearchProvider->find($request->get('type'))) {
                if ($transactionCrudManager->save($transactionFactory->buildTransactionObject($account, $transactionType, $request->get('amount')))) {
                    return (new AjaxResponse(null, Response::HTTP_CREATED))->setFlashMessage('The transaction has been successfully added')->setFlashType('success');
                }
                return (new AjaxResponse(null, Response::HTTP_INTERNAL_SERVER_ERROR))->setFlashMessage('The transaction has not been added')->setFlashType('error');
            }
            return $this->throw403();
        }

        return $this->throw404();
    }
}
