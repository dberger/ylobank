<?php

namespace App\Controller;

use App\Entity\Account;
use App\Model\Response\AjaxResponse;
use App\Services\Account\AccountRestDataFormatter;
use App\Services\Transaction\TransactionSearchProvider;
use App\Services\User\UserSearchProvider;
use MsgPhp\User\Infra\Security\SecurityUser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AccountController
 * @package App\Controller
 * @IsGranted("ROLE_USER")
 * @Route("/account")
 */
final class AccountController extends BaseController
{
    /**
     * @Route("/", name="rest_get_accounts", methods={"GET"})
     * Return all the accounts of an user
     * @param UserSearchProvider $userSearchProvider
     * @param AccountRestDataFormatter $accountRestDataFormatter
     * @return AjaxResponse
     */
    public function getAccountsOfLoggedUser(UserSearchProvider $userSearchProvider, AccountRestDataFormatter $accountRestDataFormatter)
    {
        /** @var SecurityUser $msgUser */
        $msgUser = $this->getUser();

        if (!$msgUser instanceof SecurityUser) {
            return $this->userNotLoggedIn();
        }

        /** @var \App\Entity\User\User $user */
        if ($user = $userSearchProvider->find($msgUser->getUserId()->toString())) {
            return (new AjaxResponse($accountRestDataFormatter->formatData($user->getAccounts()->toArray()), Response::HTTP_OK));
        }

        return $this->throw404();
    }

    /**
     * @Route("/total", name="rest_get_user_total_solde", methods={"GET"})
     * Return all the accounts of an user
     * @param UserSearchProvider $userSearchProvider
     * @param AccountRestDataFormatter $accountRestDataFormatter
     * @return AjaxResponse
     */
    public function getUserTotalAccountsSolde(UserSearchProvider $userSearchProvider, AccountRestDataFormatter $accountRestDataFormatter)
    {
        /** @var SecurityUser $msgUser */
        $msgUser = $this->getUser();

        if (!$msgUser instanceof SecurityUser) {
            return $this->userNotLoggedIn();
        }

        /** @var \App\Entity\User\User $user */
        if ($user = $userSearchProvider->find($msgUser->getUserId()->toString())) {
            return (new AjaxResponse($user->getTotalSolde(), Response::HTTP_OK));
        }

        return $this->throw404();
    }

    /**
     * @Route("/sum-incomes", name="rest_get_user_sum_incomes", methods={"GET"})
     * Return all the accounts of an user
     * @param UserSearchProvider $userSearchProvider
     * @param TransactionSearchProvider $transactionSearchProvider
     * @return AjaxResponse
     * @throws \Exception
     */
    public function getUserMonthlyIncome(UserSearchProvider $userSearchProvider, TransactionSearchProvider $transactionSearchProvider)
    {
        /** @var SecurityUser $msgUser */
        $msgUser = $this->getUser();

        if (!$msgUser instanceof SecurityUser) {
            return $this->userNotLoggedIn();
        }

        /** @var \App\Entity\User\User $user */
        if ($user = $userSearchProvider->find($msgUser->getUserId()->toString())) {
            return (new AjaxResponse($transactionSearchProvider->getMonthlyIncomeOfUser($user), Response::HTTP_OK));
        }

        return $this->throw404();
    }

    /**
     * @Route("/sum-outcomes", name="rest_get_user_sum_outcomes", methods={"GET"})
     * Return all the accounts of an user
     * @param UserSearchProvider $userSearchProvider
     * @param TransactionSearchProvider $transactionSearchProvider
     * @return AjaxResponse
     * @throws \Exception
     */
    public function getUserMonthlyOutcome(UserSearchProvider $userSearchProvider, TransactionSearchProvider $transactionSearchProvider)
    {
        /** @var SecurityUser $msgUser */
        $msgUser = $this->getUser();

        if (!$msgUser instanceof SecurityUser) {
            return $this->userNotLoggedIn();
        }

        /** @var \App\Entity\User\User $user */
        if ($user = $userSearchProvider->find($msgUser->getUserId()->toString())) {
            return (new AjaxResponse($transactionSearchProvider->getMonthlyIncomeOfUser($user, false), Response::HTTP_OK));
        }

        return $this->throw404();
    }

    /**
     * @Route("/sum-each-debit-type/{account}", name="rest_get_sum_each_debit_type", methods={"GET"})
     * Return all the accounts of an user
     * @param UserSearchProvider $userSearchProvider
     * @param TransactionSearchProvider $transactionSearchProvider
     * @param Account $account
     * @param Request $request
     * @return AjaxResponse
     */
    public function getEachDebitTypeForAccountAction(UserSearchProvider $userSearchProvider, TransactionSearchProvider $transactionSearchProvider, Account $account, Request $request)
    {
        $periodStart = null;
        $periodEnd = null;

        /** @var SecurityUser $msgUser */
        $msgUser = $this->getUser();

        if (!$msgUser instanceof SecurityUser) {
            return $this->userNotLoggedIn();
        }

        if ($request->get('periodStart') && $request->get('periodEnd')) {
            $periodStart = \DateTime::createFromFormat('d/m/Y', $request->get('periodStart'))->setTime(0,0,0,0);
            $periodEnd = \DateTime::createFromFormat('d/m/Y', $request->get('periodEnd'))->setTime(0,0,0,0);
            if (!$periodStart || !$periodEnd) {
                return $this->throw404();
            }
        }

        /** @var \App\Entity\User\User $user */
        if (($user = $userSearchProvider->find($msgUser->getUserId()->toString())) && $account->getUser() === $user) {
            return (new AjaxResponse($transactionSearchProvider->getSpendingsByCategoryInPeriodOfAccount($account, $periodStart, $periodEnd), Response::HTTP_OK));
        }

        return $this->throw404();
    }
}
