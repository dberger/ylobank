<?php

namespace App\Controller;

use App\Entity\TransactionType;
use App\Services\TransactionType\TransactionTypeRestDataFormatter;
use App\Services\TransactionType\TransactionTypeSearchProvider;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TransactionTypeController
 * @package App\Controller
 * @Route("/transaction-type")
 */
final class TransactionTypeController extends BaseController
{
    /**
     * @Route("/", name="rest_get_transaction_types", methods={"GET"})
     * Return all the transactionType objects available in the database
     * @param TransactionTypeSearchProvider $transactionTypeSearchProvider
     * @param TransactionTypeRestDataFormatter $transactionTypeRestDataFormatter
     * @return JsonResponse
     */
    public function getAllTransactionType(TransactionTypeSearchProvider $transactionTypeSearchProvider, TransactionTypeRestDataFormatter $transactionTypeRestDataFormatter)
    {
        return new JsonResponse($transactionTypeRestDataFormatter->formatData($transactionTypeSearchProvider->getTransactionTypes()));
    }

    /**
     * @Route("/credit", name="rest_get_transaction_types_credit", methods={"GET"})
     * Return all the transactionType objects available in the database which are credit
     * @param TransactionTypeSearchProvider $transactionTypeSearchProvider
     * @param TransactionTypeRestDataFormatter $transactionTypeRestDataFormatter
     * @return JsonResponse
     */
    public function getAllCreditTransactionType(TransactionTypeSearchProvider $transactionTypeSearchProvider, TransactionTypeRestDataFormatter $transactionTypeRestDataFormatter)
    {
        return new JsonResponse($transactionTypeRestDataFormatter->formatData($transactionTypeSearchProvider->getTransactionTypes(TransactionType::SEARCH_CREDIT)));
    }

    /**
     * @Route("/debit", name="rest_get_transaction_types_debit", methods={"GET"})
     * Return all the transactionType objects available in the database which are debit
     * @param TransactionTypeSearchProvider $transactionTypeSearchProvider
     * @param TransactionTypeRestDataFormatter $transactionTypeRestDataFormatter
     * @return JsonResponse
     */
    public function getAllDebitTransactionType(TransactionTypeSearchProvider $transactionTypeSearchProvider, TransactionTypeRestDataFormatter $transactionTypeRestDataFormatter)
    {
        return new JsonResponse($transactionTypeRestDataFormatter->formatData($transactionTypeSearchProvider->getTransactionTypes(TransactionType::SEARCH_DEBIT)));
    }

}
