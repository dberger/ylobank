<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\AccountType;
use App\Entity\TransactionType;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20190809123723
 * @package DoctrineMigrations
 * Init the database
 */
final class Version20190809123723 extends AbstractMigration
{
    /**
     * @return string
     */
    public function getDescription(): string
    {
        return 'Init database';
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\DBALException
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE account (id INT AUTO_INCREMENT NOT NULL, id_account_type INT NOT NULL, id_user INT NOT NULL COMMENT \'(DC2Type:msgphp_user_id)\', solde NUMERIC(7, 2) NOT NULL, INDEX IDX_7D3656A4238CA5F1 (id_account_type), INDEX IDX_7D3656A46B3CA4B (id_user), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE account_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, interest_percentage NUMERIC(7, 2) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transaction (id INT AUTO_INCREMENT NOT NULL COMMENT \'Id of transaction\', id_account INT NOT NULL, id_transaction_type INT NOT NULL, amount NUMERIC(7, 2) NOT NULL, discr VARCHAR(255) NOT NULL, INDEX IDX_723705D1A3ABFFD4 (id_account), INDEX IDX_723705D18D173C59 (id_transaction_type), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB COMMENT = \'Table of transactions\' ');
        $this->addSql('CREATE TABLE transaction_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role (name VARCHAR(191) NOT NULL, PRIMARY KEY(name)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL COMMENT \'(DC2Type:msgphp_user_id)\', credential_email VARCHAR(191) NOT NULL, credential_password VARCHAR(255) NOT NULL, password_reset_token VARCHAR(191) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649A5D24B55 (credential_email), UNIQUE INDEX UNIQ_8D93D6496B7BA4B6 (password_reset_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_role (user_id INT NOT NULL COMMENT \'(DC2Type:msgphp_user_id)\', role_name VARCHAR(191) NOT NULL, INDEX IDX_2DE8C6A3A76ED395 (user_id), INDEX IDX_2DE8C6A3E09C0C92 (role_name), PRIMARY KEY(user_id, role_name)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE account ADD CONSTRAINT FK_7D3656A4238CA5F1 FOREIGN KEY (id_account_type) REFERENCES account_type (id)');
        $this->addSql('ALTER TABLE account ADD CONSTRAINT FK_7D3656A46B3CA4B FOREIGN KEY (id_user) REFERENCES user (id)');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D1A3ABFFD4 FOREIGN KEY (id_account) REFERENCES account (id)');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D18D173C59 FOREIGN KEY (id_transaction_type) REFERENCES transaction_type (id)');
        $this->addSql('ALTER TABLE user_role ADD CONSTRAINT FK_2DE8C6A3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_role ADD CONSTRAINT FK_2DE8C6A3E09C0C92 FOREIGN KEY (role_name) REFERENCES role (name) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE transaction ADD date_transaction DATETIME NOT NULL');

        $this->initAccountType();
        $this->initTransactionType();
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\DBALException
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D1A3ABFFD4');
        $this->addSql('ALTER TABLE account DROP FOREIGN KEY FK_7D3656A4238CA5F1');
        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D18D173C59');
        $this->addSql('ALTER TABLE user_role DROP FOREIGN KEY FK_2DE8C6A3E09C0C92');
        $this->addSql('ALTER TABLE account DROP FOREIGN KEY FK_7D3656A46B3CA4B');
        $this->addSql('ALTER TABLE user_role DROP FOREIGN KEY FK_2DE8C6A3A76ED395');
        $this->addSql('DROP TABLE account');
        $this->addSql('DROP TABLE account_type');
        $this->addSql('DROP TABLE transaction');
        $this->addSql('DROP TABLE transaction_type');
        $this->addSql('DROP TABLE role');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_role');
    }

    /**
     * Create in database the different types of account
     */
    private function initAccountType()
    {
        foreach (AccountType::getAccountsConstants() as $key => $value) {
            if ($name = AccountType::getAccountTypeLibelleWithCode($key)) {
                $this->addSql("INSERT INTO account_type (name,code, interest_percentage) VALUES ('{$name}' ,'{$key}', '{$value}')");
            }
        }
    }

    /**
     * Create in database the different types of transaction
     */
    private function initTransactionType()
    {
        foreach (TransactionType::getDefaultValues() as $key => $value) {
            $this->addSql("INSERT INTO transaction_type (name,code) VALUES ('{$value}', '{$key}')");
        }
    }
}
