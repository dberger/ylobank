<?php

namespace App\DataProviders;

use App\Entity\User\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserFactory
 * @package App\DataProviders
 */
final class UserFactory
{
    /**
     * @var UserPasswordEncoder
     */
    private $encoder;

    /**
     * PasswordProvider constructor.
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @param $entity
     * @param $password
     * @return string
     */
    public function pwdEncode($entity, $password)
    {
        return $this->encoder->encodePassword($entity, $password);
    }

    /**
     * @param $userId
     * @param $email
     * @param $password
     * @return User
     */
    public function createUser($userId, $email, $password)
    {
        $user = new User($userId, $email, '');
        $user->changePassword($this->pwdEncode($user, $password));
        return $user;
    }
}
