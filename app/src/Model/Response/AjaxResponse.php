<?php

namespace App\Model\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class AjaxResponse
 * @package App\Model\Response
 */
final class AjaxResponse extends JsonResponse
{
    const JS_FUNCTION_RELOAD_BLOCK_AJAX = 'initAjaxReload';
    const JS_FUNCTION_CLOSE_MODAL = 'closeModal';
    const JS_FUNCTION_RELOAD_DATATABLE = 'reloadDatatables';
    const JS_FUNCTION_RELOAD_PAGE = 'reloadPage';

    /**
     * @var array
     */
    private $callbacks; // fonctions callback en JS

    /**
     * @var array
     */
    private $arguments;

    /**
     * @var string
     */
    private $flashType;

    /**
     * @var string
     */
    private $flashMessage;

    /**
     * AjaxResponse constructor.
     * @param array $data
     * @param int $status
     * @param array $callbacks
     * @param array $arguments
     * @param string $flashType
     * @param string $flashMessage
     */
    public function __construct($data = null, $status = 200, $callbacks = [], $arguments = [], $flashType = '', $flashMessage = '')
    {
        parent::__construct($data, $status, [], false);

        $this->callbacks = $callbacks;
        $this->arguments = $arguments;
        $this->flashType = $flashType;
        $this->flashMessage = $flashMessage;
    }

    /**
     * @return array
     */
    public function getCallbacks()
    {
        return $this->callbacks;
    }

    /**
     * @param array $callbacks
     * @return AjaxResponse
     */
    public function setCallbacks($callbacks)
    {
        $this->callbacks = $callbacks;
        return $this;
    }

    /**
     * @return array
     */
    public function getArguments()
    {
        return $this->arguments;
    }

    /**
     * @param array $arguments
     * @return AjaxResponse
     */
    public function setArguments($arguments)
    {
        $this->arguments = $arguments;
        return $this;
    }

    /**
     * @return string
     */
    public function getFlashType()
    {
        return $this->flashType;
    }

    /**
     * @param string $flashType
     * @return AjaxResponse
     */
    public function setFlashType($flashType)
    {
        $this->flashType = $flashType;
        return $this;
    }

    /**
     * @return string
     */
    public function getFlashMessage()
    {
        return $this->flashMessage;
    }

    /**
     * @param string $flashMessage
     * @return AjaxResponse
     */
    public function setFlashMessage($flashMessage)
    {
        $this->flashMessage = $flashMessage;
        return $this;
    }

    /**
     * @return AjaxResponse
     */
    public function formatResponseContent()
    {
        $result = [];
        $result['content'] = $this->data;
        $result['data']['callbacks'] = $this->getCallbacks();
        $result['data']['arguments'] = $this->getArguments();
        $result['data']['flash_type'] = $this->getFlashType();
        $result['data']['flash_message'] = $this->getFlashMessage();

        $this->setData($result);

        return $this;
    }

    /**
     * @param string $type
     * @param string $message
     *
     * @return AjaxResponse
     */
    public function formatFlashMessage($type, $message)
    {
        return $this
            ->setFlashType($type)
            ->setFlashMessage($message);
    }
}
