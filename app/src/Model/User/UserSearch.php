<?php

namespace App\Model\User;

/**
 * Class UserSearch
 * @package App\Model\User
 */
final class UserSearch
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return UserSearch
     */
    public function setId(int $id): UserSearch
    {
        $this->id = $id;
        return $this;
    }
}
