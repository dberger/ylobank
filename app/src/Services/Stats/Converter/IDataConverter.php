<?php

namespace App\Services\Stats\Converter;

/**
 * Interface IDataConverter
 * @package App\Services\Stats\Converter
 */
interface IDataConverter
{
    /**
     * @param array $data
     * @return array
     */
    public function convert(array $data) : array;
}
