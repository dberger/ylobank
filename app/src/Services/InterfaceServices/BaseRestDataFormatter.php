<?php

namespace App\Services\InterfaceServices;

/**
 * Interface BaseRestDataFormatter
 * @package App\Services\InterfaceServices
 */
interface BaseRestDataFormatter
{
    /**
     * Format the objects as Json data
     * @param array $objects
     * @return array
     */
    public function formatData(array $objects) : array;
}
