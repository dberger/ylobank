<?php

namespace App\Services\TransactionType;

use App\Repository\TransactionTypeRepository;
use App\Services\AbstractServices\BaseSearchProvider;

/**
 * Class TransactionTypeSearchProvider
 * @package App\Services\TransactionType
 */
final class TransactionTypeSearchProvider extends BaseSearchProvider
{
    /**
     * TransactionTypeSearchProvider constructor.
     * @param TransactionTypeRepository $transactionTypeRepository
     */
    public function __construct(TransactionTypeRepository $transactionTypeRepository)
    {
        parent::__construct($transactionTypeRepository);
    }

    /**
     * Return only the objects that are for the debit form
     * @param string|null $type
     * @return array
     */
    public function getTransactionTypes(string $type = null)
    {
        /** @var TransactionTypeRepository $repo */
        $repo = $this->entityRepository;
        return $repo->getTypes($type)->execute();
    }
}
