<?php

namespace App\Services\TransactionType;

use App\Entity\TransactionType;
use App\Services\InterfaceServices\BaseRestDataFormatter;

/**
 * Class TransactionTypeRestDataFormatter
 * @package App\Services\TransactionType
 */
final class TransactionTypeRestDataFormatter implements BaseRestDataFormatter
{
    /**
     * Format the objects as Json data
     * @param array $objects
     * @return array
     */
    public function formatData(array $objects): array
    {
        /** @var TransactionType[] $objects */
        $results = [];

        foreach ($objects as $transactionType) {
            $results[] = [
                'id' => $transactionType->getId(),
                'name' => $transactionType->getName(),
                'code' => $transactionType->getCode()
            ];
        }

        return $results;
    }
}
