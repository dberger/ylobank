<?php

namespace App\Services\Transaction;

use App\Entity\Account;
use App\Entity\User\User;
use App\Repository\TransactionRepository;
use App\Services\AbstractServices\BaseSearchProvider;
use App\Services\Stats\Converter\DoughutDataConverter;

/**
 * Class TransactionSearchProvider
 * @package App\Services\Transaction
 */
final class TransactionSearchProvider extends BaseSearchProvider
{
    /**
     * @var DoughutDataConverter
     */
    private $doughutDataConverter;

    /**
     * TransactionTypeSearchProvider constructor.
     * @param TransactionRepository $transactionRepository
     * @param DoughutDataConverter $doughutDataConverter
     */
    public function __construct(TransactionRepository $transactionRepository, DoughutDataConverter $doughutDataConverter)
    {
        parent::__construct($transactionRepository);
        $this->doughutDataConverter = $doughutDataConverter;
    }

    /**
     * @param User $user
     * @param bool $isIncome
     * @return float
     * @throws \Exception
     */
    public function getMonthlyIncomeOfUser(User $user, $isIncome = true)
    {
        /** @var TransactionRepository $er */
        $er = $this->entityRepository;
        return $er->getMonthlyIncomeOrOutcomeOfUser($user, $isIncome)->getSingleScalarResult();
    }

    /**
     * @param Account $account
     * @param \DateTime|null $periodStart
     * @param \DateTime|null $periodEnd
     * @return array
     */
    public function getSpendingsByCategoryInPeriodOfAccount(Account $account, ?\DateTime $periodStart, ?\DateTime $periodEnd)
    {
        /** @var TransactionRepository $er */
        $er = $this->entityRepository;
        return $this->doughutDataConverter->convert($er->getSpendingsByCategoryInPeriodOfAccount($account, $periodStart, $periodEnd)->execute());
    }
}
