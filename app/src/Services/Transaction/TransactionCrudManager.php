<?php

namespace App\Services\Transaction;

use App\Entity\Account;
use App\Entity\Transaction\Credit;
use App\Entity\Transaction\Debit;
use App\Entity\Transaction\Transaction;
use App\Entity\TransactionType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class TransactionFactory
 * @package App\Services\Transaction
 */
final class TransactionCrudManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * TransactionCrudManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Transaction $transaction
     * @return Transaction
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Transaction $transaction)
    {
        $this->entityManager->persist($transaction);
        $this->entityManager->flush();
        return $transaction;
    }

}
