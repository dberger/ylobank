<?php

namespace App\Services\Transaction;

use App\Entity\Account;
use App\Entity\Transaction\Credit;
use App\Entity\Transaction\Debit;
use App\Entity\Transaction\Transaction;
use App\Entity\TransactionType;

/**
 * Class TransactionFactory
 * @package App\Services\Transaction
 */
final class TransactionFactory
{
    /**
     * @param Account $account
     * @param TransactionType $transactionType
     * @param float $amount
     * @return Transaction
     * @throws \Exception
     */
    public function buildTransactionObject(Account $account, TransactionType $transactionType, float $amount)
    {
        if (key_exists($transactionType->getCode(), TransactionType::getCreditTypes())) {
            return (new Credit())->setAmount($amount)->setTransactionType($transactionType)->setAccount($account);
        }
        return (new Debit())->setAmount($amount)->setTransactionType($transactionType)->setAccount($account);
    }
}
