<?php

namespace App\Services\AccountType;

use App\Repository\AccountTypeRepository;
use App\Services\AbstractServices\BaseSearchProvider;

/**
 * Class AccountTypeSearchProvider
 * @package App\Services\AccountType
 */
final class AccountTypeSearchProvider extends BaseSearchProvider
{
    /**
     * AccountTypeSearchProvider constructor.
     * @param AccountTypeRepository $accountTypeRepository
     */
    public function __construct(AccountTypeRepository $accountTypeRepository)
    {
        parent::__construct($accountTypeRepository);
    }
}
