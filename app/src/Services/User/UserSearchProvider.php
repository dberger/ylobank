<?php

namespace App\Services\User;

use App\Repository\UserRepository;
use App\Services\AbstractServices\BaseSearchProvider;

/**
 * Class UserSearchProvider
 * @package App\Services\User
 */
final class UserSearchProvider extends BaseSearchProvider
{
    /**
     * UserSearchProvider constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        parent::__construct($userRepository);
    }
}
