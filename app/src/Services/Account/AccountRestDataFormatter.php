<?php

namespace App\Services\Account;


use App\Entity\Account;
use App\Entity\AccountType;
use App\Services\InterfaceServices\BaseRestDataFormatter;

/**
 * Class AccountRestDataFormatter
 * @package App\Services\Account
 */
final class AccountRestDataFormatter implements BaseRestDataFormatter
{
    /**
     * Format the objects as Json data
     * @param array $objects
     * @return array
     */
    public function formatData(array $objects): array
    {
        /** @var Account[] $objects */
        $results = [];
        $i = 0;

        foreach ($objects as $account) {
            $accountData = [
                'id' => $account->getId(),
                'type' => $account->getAccountType() ? $account->getAccountType()->getName() : 'NC',
                'solde' => $account->getSolde(),
                'codeType' => $account->getAccountType() ? $account->getAccountType()->getCode() : 'NC'
            ];

            $transactions = [];

            foreach ($account->getTransactions() as $transaction) {
                $transactions[] = [
                    'id' => $transaction->getId(),
                    'amount' => $transaction->getAmount(),
                    'type' => $transaction->getTransactionType() ? $transaction->getTransactionType()->getName() : 'NC'
                ];
            }

            if ($account->getAccountType()->getCode() === AccountType::ACC_TYPE_CPT_CHEQUE && $i !== 0) {
                $old = $results[0];
                $results[0] = array_merge($accountData, ['transactions' => $transactions]);
                $results[$i] = $old;
            } else {
                $results[] = array_merge($accountData, ['transactions' => $transactions]);
            }

            $i++;
        }

        return $results;
    }
}
