<?php

namespace App\Services\Account;

use App\Entity\User\User;
use App\Repository\AccountRepository;
use App\Services\AbstractServices\BaseSearchProvider;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

/**
 * Class AccountTypeSearchProvider
 * @package App\Services\AccountType
 */
final class AccountSearchProvider extends BaseSearchProvider
{
    /**
     * AccountTypeSearchProvider constructor.
     * @param AccountRepository $accountRepository
     */
    public function __construct(AccountRepository $accountRepository)
    {
        parent::__construct($accountRepository);
    }

    public function getAccountWithUserConstraint(User $user, int $accountId)
    {
        /** @var AccountRepository $repo */
        $repo = $this->entityRepository;
        try {
            return $repo->getAccountWithUserConstraint($user, $accountId)->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
