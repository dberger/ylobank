<?php

namespace App\Services\AbstractServices;

use Doctrine\ORM\EntityRepository;

/**
 * Class BaseSearchProvider
 * @package App\Services\AbstractServices
 */
abstract class BaseSearchProvider
{
    /**
     * @var EntityRepository
     */
    protected $entityRepository;

    /**
     * BaseSearchProvider constructor.
     * @param EntityRepository $entityRepository
     */
    public function __construct(EntityRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }

    /**
     * @return array
     */
    public function findAll()
    {
        return $this->entityRepository->findAll();
    }

    /**
     * @param int $id
     * @return object|null
     */
    public function find(int $id)
    {
        return $this->entityRepository->find($id);
    }
}
