<?php

namespace App\Tests\Entity\Account;

use App\Entity\Account;
use App\Entity\Transaction\Credit;
use App\Entity\Transaction\Debit;
use PHPUnit\Framework\TestCase;

/**
 * Class AccountTest
 * @package App\Tests\Entity\Account
 */
final class AccountTest extends TestCase
{
    /***
     * @var Account
     */
    private $account;

    /**
     * Basic Setup
     */
    public function setUp(){
        // We create a new bank account with 1000 €
        $this->account = (new Account())->setSolde(1000);
    }

    /**
     * Testing to add a Credit object to the bank account
     */
    public function testAddCredit(){
        $oldSolde = $this->account->getSolde();
        $creditAmount = 550.50;

        $credit = (new Credit())->setAmount($creditAmount)->setAccount($this->account);
        $this->account->addTransaction($credit);
        $this->assertEquals($this->account->getSolde(), $oldSolde + $creditAmount);
    }

    /**
     * Testing to add a Debit object to the bank account
     */
    public function testAddDebit(){
        $oldSolde = $this->account->getSolde();
        $debitAmount = 150.50;

        $debit = (new Debit())->setAmount($debitAmount)->setAccount($this->account);
        $this->account->addTransaction($debit);
        $this->assertEquals($this->account->getSolde(), $oldSolde - $debitAmount);
    }

    /**
     * Testing to remove a Credit object to the bank account
     */
    public function testRemoveCredit(){
        $oldSolde = $this->account->getSolde();
        $creditAmount = 550.50;

        $credit = (new Credit())->setAmount($creditAmount)->setAccount($this->account);
        $this->account->addTransaction($credit);
        $this->account->removeTransaction($credit);

        $this->assertEquals($this->account->getSolde(), $oldSolde);
    }

    /**
     * Testing to remove a Debit object to the bank account
     */
    public function testRemoveDebit(){
        $oldSolde = $this->account->getSolde();
        $creditAmount = 150.50;

        $debit = (new Debit())->setAmount($creditAmount)->setAccount($this->account);
        $this->account->addTransaction($debit);
        $this->account->removeTransaction($debit);

        $this->assertEquals($this->account->getSolde(), $oldSolde);
    }
}
