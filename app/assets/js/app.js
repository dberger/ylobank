var $ = require('jquery');
global.$ = $;
global.jQuery = $;
require('../scss/app.scss');
require('bootstrap');
require('select2');
require('admin-lte');
require('jquery.countdown');
require('datatables/media/js/jquery.dataTables.min');
require('datatables/media/css/jquery.dataTables.min.css');
require('font-awesome/scss/font-awesome.scss');

var app = {};
global.app = app;
