up:
	docker-compose up -d --build

down:
	docker-compose down

php:
	docker exec -it ylobank_php-fpm /bin/bash
